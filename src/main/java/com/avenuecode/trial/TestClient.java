package com.avenuecode.trial;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;


public class TestClient {

    private static final String BASE_URI = "http://localhost:9090";
    private WebTarget target;

    public TestClient() {
        Client client = ClientBuilder.newClient();
        target = client.target(BASE_URI + "/produto");
    }

    public void ping() {
        WebTarget path = target.path("/ping");
        Response response = path.request().get();
        System.out.println("target.ping: " + response.readEntity(String.class));
    }    


    public void prodImg() {
        WebTarget path = target.path("/1/imgs");
        Response response = path.request().get();
        System.out.println("target.img: " + response.readEntity(String.class));
    }
    public void prodSub() {
        WebTarget path = target.path("/3/sub");
        Response response = path.request().get();
        System.out.println("target.sub: " + response.readEntity(String.class));
    }
    public void prodComplete() {
        WebTarget path = target.path("/1/complete");
        Response response = path.request().get();
        System.out.println("target.complete: " + response.readEntity(String.class));
    }
    public void prod() {
        WebTarget path = target.path("/1");
        Response response = path.request().get();
        System.out.println("target: " + response.readEntity(String.class));
    }
    public void listComplete() {
        WebTarget path = target.path("/list/complete");
        Response response = path.request().get();
        System.out.println("target.biglist: " + response.readEntity(String.class));
    }
    public void list() {
        WebTarget path = target.path("/list");
        Response response = path.request().get();
        System.out.println("target.list: " + response.readEntity(String.class));
    }

    public void exit() {
        try {
        	target.path("/shutdown").request().get();
        } catch (Throwable t) {
        	Logger.getLogger(TestClient.class.getName())
        		.log(Level.WARNING, t.getMessage(), t);
        }
    }

    public static void main(String[] args) {
    	System.out.println("Client de teste: requer StartServer inicializado antes.");
    	TestClient wc = new TestClient();
        wc.ping();
        wc.list();
        wc.listComplete();
        wc.prod();
        wc.prodComplete();
        wc.prodImg();
        wc.prodSub();
        wc.exit();
        System.out.println("end");
        System.exit(0);
    }
}
