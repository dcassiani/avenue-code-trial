package com.avenuecode.trial;

import java.net.URI;

import org.glassfish.grizzly.Connection;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.HttpServerFilter;
import org.glassfish.grizzly.http.server.HttpServerProbe;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import com.avenuecode.trial.endpoint.impl.ProductEndpointImpl;



public class StartServer {

    private static final String BASE_URL = "http://localhost:9090/";

    public static void main(String[] args) throws Exception {
        
        System.out.println("Iniciando server em: " + BASE_URL);

        final ResourceConfig resourceConfig = new ResourceConfig();
        resourceConfig.register(ProductEndpointImpl.class);

        HttpServer server = GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URL), resourceConfig, false);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            server.shutdownNow();
        }));

        HttpServerProbe probe = new HttpServerProbe.Adapter() {
            public void onRequestReceiveEvent(HttpServerFilter filter, Connection connection, Request request) {
                System.out.println(request.getRequestURI());
            }
        };
        server.getServerConfiguration().getMonitoringConfig().getWebServerConfig().addProbes(probe);
        server.start();
        System.out.println("Server iniciado em: " + BASE_URL);
        Thread.currentThread().join();
        server.shutdown();

    }
}
