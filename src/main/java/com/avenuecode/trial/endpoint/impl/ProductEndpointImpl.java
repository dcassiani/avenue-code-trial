package com.avenuecode.trial.endpoint.impl;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.avenuecode.trial.dao.ProductDAO;
import com.avenuecode.trial.dao.impl.ProductDAOImpl;
import com.avenuecode.trial.data.ProductVO;
import com.avenuecode.trial.endpoint.ProductEndpoint;



@Path("/produto")
public class ProductEndpointImpl implements ProductEndpoint {
    public final static Logger LOGGER = Logger.getLogger(ProductEndpoint.class.getName());


    @Override
    public Response ping() {
        return Response.status(Response.Status.OK).entity("PONG").build();
    }


    @Override
    public Response shutdown() {
        System.exit(0);
        return Response.noContent().build();
    }




	@Override
	public Response getListCompleteProducts() {
		try{
			ProductDAO dao = new ProductDAOImpl(); 
			Collection<ProductVO> vo = dao.getListCompleteProducts();
			return Response.status(Response.Status.OK).entity(vo).build();
		} catch (Exception e){
			return internalServerError(e);
		}
	}


	@Override
	public Response getListProducts() {
		try{
			ProductDAO dao = new ProductDAOImpl(); 
			Collection<ProductVO> vo = dao.getListProducts();
			return Response.status(Response.Status.OK).entity(vo).build();
		} catch (Exception e){
			return internalServerError(e);
		}
	}


	@Override
	public Response getProduct(Integer idProd) {
		try{
			ProductDAO dao = new ProductDAOImpl(); 
			ProductVO vo = dao.getProduct(idProd);
			return Response.status(Response.Status.OK).entity(vo).build();
		} catch (Exception e){
			return internalServerError(e);
		}
	}


	@Override
	public Response getCompleteProduct(Integer idProd) {
		try{
			ProductVO vo = getCompleteProductDAO(idProd);
			return Response.status(Response.Status.OK).entity(vo).build();
		} catch (Exception e){
			return internalServerError(e);
		}
	}


	private Response internalServerError(Exception e) {
		e.printStackTrace();
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
				.entity(e.getMessage()).build();
	}


	private ProductVO getCompleteProductDAO(Integer idProd) throws Exception {
		ProductDAO dao = new ProductDAOImpl(); 
		ProductVO vo = dao.getCompleteProduct(idProd);
		return vo;
	}

	

	@Override
	public Response getSubProducts(Integer idProd) {
		try{
			ProductVO vo = getCompleteProductDAO(idProd);
			return Response.status(Response.Status.OK).entity(vo.getParentProduct()).build();
		} catch (Exception e){
			return internalServerError(e);
		}
	}


	@Override
	public Response getProductImages(Integer idProd) {
		try{
			ProductVO vo = getCompleteProductDAO(idProd);
			return Response.status(Response.Status.OK).entity(vo.getImg()).build();
		} catch (Exception e){
			return internalServerError(e);
		}
	}

 
}
