package com.avenuecode.trial.endpoint;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
a)Get all products excluding relationships (child products, images)
b)Get all products including specified relationships (child product and/or images)
c)Same as 1 using specific product identity
d)Same as 2 using specific product identity
e)Get set of child products for specific product
f)Get set of images for specific product
 *
 */

public interface ProductEndpoint {

    /**
     * Get all products excluding relationships (child products, images)
     */
    @GET
    @Path("/list")
    @Produces(MediaType.APPLICATION_JSON)
    Response getListProducts();

    /**
     * Get all products including specified relationships (child product and/or images)
     */
    @GET
    @Path("/list/complete")
    @Produces(MediaType.APPLICATION_JSON)
    Response getListCompleteProducts();

    /**
     * Same as 1 using specific product identity
     */
    @GET
    @Path("/{idProd}")
    @Produces(MediaType.APPLICATION_JSON)
    Response getProduct(@PathParam("idProd") Integer idProd);

    /**
     * Same as 2 using specific product identity
     */
    @GET
    @Path("/{idProd}/complete")
    @Produces(MediaType.APPLICATION_JSON)
    Response getCompleteProduct(@PathParam("idProd") Integer idProd);
    
    
    /**
     * Get set of child products for specific product
     */
    @GET
    @Path("/{idProd}/sub")
    @Produces(MediaType.APPLICATION_JSON)
    Response getSubProducts(@PathParam("idProd") Integer idProd);
    
    /**
     * Get set of images for specific product
     */
    @GET
    @Path("/{idProd}/imgs")
    @Produces(MediaType.APPLICATION_JSON)
    Response getProductImages(@PathParam("idProd") Integer idProd);
    
    /**
     * fun��o PING para teste do endpoint. N�o existiria no mundo real.
     */
    @GET
    @Path("/ping")
    Response ping();
    
    
    /**
     * desliga o servidor para teste do endpoint. N�o existiria no mundo real.
     */
    @GET
    @Path("/exit")
    Response shutdown();
}
