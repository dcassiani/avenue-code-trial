package com.avenuecode.trial.data;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="product")
public class ProductVO implements Serializable{
	private static final long serialVersionUID = 2573685505033495299L;

	public ProductVO() {	}
	
	public ProductVO(int id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	@Column
	private String name;
	
	@Column
	private String description;
	
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name = "parent_product_id")
	private ProductVO parentProduct;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name = "product_id")
	private Collection<ImageVO> img;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public ProductVO getParentProduct() {
		return parentProduct;
	}
	public void setParentProduct(ProductVO parentProduct) {
		this.parentProduct = parentProduct;
	}
	public Collection<ImageVO> getImg() {
		return img;
	}
	public void setImg(Collection<ImageVO> img) {
		this.img = img;
	}
	
}
