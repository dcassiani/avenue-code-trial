package com.avenuecode.trial.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="image")
public class ImageVO implements Serializable{
	private static final long serialVersionUID = -7528101903220037120L;
	
	protected ImageVO() {	}
	
	@Id
	private Integer id;
	
	@Column
	private String type;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
