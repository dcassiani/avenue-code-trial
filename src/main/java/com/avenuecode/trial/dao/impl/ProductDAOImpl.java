package com.avenuecode.trial.dao.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.avenuecode.trial.dao.ProductDAO;
import com.avenuecode.trial.data.ProductVO;

public class ProductDAOImpl implements ProductDAO{

	
	
	@Override
	public Collection<ProductVO> getListCompleteProducts() throws Exception{
		Collection<ProductVO> ret = new ArrayList<ProductVO>();
		try{
			Session s = HibernateFactory.getSession();
			ret.addAll(s.createCriteria(ProductVO.class)
					.setReadOnly(true)
					.list());
	    	s.close();
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return ret;
	}

	@Override
	public Collection<ProductVO> getListProducts() throws Exception {
		Collection<ProductVO> ret = new ArrayList<ProductVO>();
		try{
			Session s = HibernateFactory.getSession();
			ret.addAll(s.createQuery("select new ProductVO(prod.id, prod.name, "
					+ "prod.description) from ProductVO prod")
					.setReadOnly(true)
					.list());
	    	s.close();
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return ret;
	}

	@Override
	public ProductVO getCompleteProduct(Integer idProd) throws Exception {
		ProductVO ret = null;
		try{
			Session s = HibernateFactory.getSession();
			ret = (ProductVO) s.createCriteria(ProductVO.class)
					.add( Restrictions.idEq(idProd))
					.setReadOnly(true)
					.uniqueResult();
	    	s.close();
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return ret;
	}

	@Override
	public ProductVO getProduct(Integer idProd) throws Exception {
		ProductVO ret = null;
		try{
			Session s = HibernateFactory.getSession();
			ret = (ProductVO) s.createQuery("select new ProductVO(prod.id, prod.name, prod.description) "
					+ "from ProductVO prod where prod.id = :idProd")
					.setParameter("idProd", idProd)
					.setReadOnly(true)
					.uniqueResult();
	    	s.close();
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return ret;
	}

 
}
