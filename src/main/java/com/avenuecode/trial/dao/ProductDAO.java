package com.avenuecode.trial.dao;

import java.util.Collection;

import com.avenuecode.trial.data.ProductVO;

public interface ProductDAO {

	public Collection<ProductVO> getListCompleteProducts() throws Exception;

	public Collection<ProductVO> getListProducts() throws Exception;

	public ProductVO getCompleteProduct(Integer idProd) throws Exception;

	public ProductVO getProduct(Integer idProd) throws Exception;
}
